# GitHub 入门与实践

## 相关名词

1.GitHub：是为开发者提供Git仓库的托管服务

2.GitHub Flavored Markdown（GFM）

在GitHub上，用户所有用文字输入的功能都可以用GFM语法进行描述。

3.social coding

面向全世界的代码公开必将越发重要，以编写代码为生的职业程序员们，更应该进行社会化编程。

4.Issue

是将一个任务或问题分配以进行追踪和管理的功能，可以像BUG管理系统或TIDD（ticket-driven Development）
的ticket一样使用。

5.Git 

Linux的创始人Linus Torvalds在2005年开发了Git的原型程序。当时，由于在Linux内核开发中使用的既有版本
管理系统的开发方许可证发生了变更 

6.Subversion

在Git出现以前，人们普遍采用Subversion等中型版本管理系统。

7.Pull Request

是指开发者在本地对源代码进行更改后，想GitHub中托管的Git仓库请求合并的功能。

## 功能介绍

1.Repositories you contribute to 
显示用户做过贡献的仓库

2.Pulse
显示该仓库最近的活动信息。该仓库中的软件是无人问津，还是在火热地开发之中，从这里可以一目了然。

3.Issue
用于BUG报告，功能添加，方向性讨论等，将这些以Issue 形式进行管理。Pull Resquest时也会创建Issue。
旁边显示的数字是当前处于Open状态的Issue数。

4.Star
旁边的数字表示给这个仓库添加星星的人数。更像是书签，让用户将来可以在Star标记的列表中找到该仓库，另外，
Star数还是GitHub上判断仓库热门程度的指标之一。

5.Fork
后面的数字表示该仓库被Fork至个用户仓库的次数，这个数字越大，表示参与这个仓库开发的人越多。

6.Gist
主要用于管理及发布一些没必要保存在仓库中的代码，比如小的代码片段等。系统会自动管理更新历史，并且提供了Fork
功能。另外，通过Gist还可以很方便地为同事编写示例。

7.Pull Request
代码的更改和讨论都在这里进行。旁边显示的数字表示尚未close的pull requests 的数量

8.News Feed
显示当前已follow的用户和已watch的项目的活动信息，用户可以在这里查看最新动向。将右上
角Rss标志的URL添加到RSS阅读器中，还可以通过RSS查看

9.watch 
点击这个按钮就可以watch该仓库，今后该仓库的更新信息会显示在用户的公众活动中，之后该仓库的相关信息会在后续的notifications
中显示，让用户可以追踪仓库的内容。

10.Wiki
常用于记录开发者之间应该共享的信息或软件文档。数字表示当前Wiki的页面数量。

11.Account Settings
图标是一把螺丝刀和一柄锤子，点击可以打开账户设置页面，在这里可以进行个人信息，安全管理，付费方案的设置，
使用GitHub时务必浏览。

### 网址信息

1.https://github.com/nfu_newmedia

无关或错误

2.https://github.com/hanteng

可以看个人信息

3.https://github.com/hantenng /nfu_newmedia/comits/

访问代码提交史列表

4.https://nfu_newmedia.github.io/

无关或错误

5.https://github.com/hangteng /nfu_newmedia

可以看某人某仓库

6.https://github.com/hanteng/nfu_newmedia/pull/1

访问某pull request

7.https://github.com/hanteng/nfu_newmedia/pulls

访问pull request列表

8.https://hanteng.github.io/

使用GitHub Page 的前端产出页面

9.https://github.com/hanteng/nfu_newmedia/pull request/1

 无关或错误







